package com.iserveu.aeps.matmtransaction;

import com.iserveu.aeps.microatm.MicroAtmRequestModel;
import com.iserveu.aeps.microatm.MicroAtmResponse;
import com.iserveu.aeps.transaction.ReportRequest;
import com.iserveu.aeps.transaction.ReportResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface MicroReportApi {

    @POST("/transactiondetails")
    Call<MicroReportResponse> insertUser(@Header("Authorization") String token, @Body MicroReportRequest body);

}
