package com.iserveu.aeps.dashboard;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface BalanceApi {
        @GET("/user/dashboard")
        Call<BalanceResponse> getBalanceDetails(@Header("Authorization") String token);
    }


