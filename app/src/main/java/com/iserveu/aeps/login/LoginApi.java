package com.iserveu.aeps.login;

import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface LoginApi {
    @POST("/aeps/userlogin")

    Call<LoginResponse> insertUser(@Header("Authorization") String token);
}
