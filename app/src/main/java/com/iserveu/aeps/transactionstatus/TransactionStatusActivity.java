package com.iserveu.aeps.transactionstatus;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iserveu.aeps.R;
import com.iserveu.aeps.utils.Constants;

public class TransactionStatusActivity extends AppCompatActivity {
// Card transaction sucessfully done for the card number . \n \n Reference No : 536645545545 \n Available Balance : 6466.98 \n Transaction Amount : 546.00 \n Transaction Date and TIme : 2018-11-06 13:26:03 \n Terminal ID : IS000004
    LinearLayout successLayout,failureLayout;
    Button okButton,okSuccessButton;
    TextView detailsTextView,failureDetailTextView,failureTitleTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_status);
        successLayout = findViewById(R.id.successLayout);
        failureLayout = findViewById(R.id.failureLayout);
        okButton = findViewById(R.id.okButton);
        okSuccessButton = findViewById(R.id.okSuccessButton);
        detailsTextView = findViewById(R.id.detailsTextView);
        failureTitleTextView = findViewById(R.id.failureTitleTextView);
        failureDetailTextView = findViewById(R.id.failureDetailTextView);

        if(getIntent().getSerializableExtra(Constants.TRANSACTION_STATUS_KEY) == null){
            failureLayout.setVisibility(View.VISIBLE);
            successLayout.setVisibility(View.GONE);
            failureDetailTextView .setText ( "Some Exception occured");
        }else{
            TransactionStatusModel transactionStatusModel = (TransactionStatusModel) getIntent().getSerializableExtra(Constants.TRANSACTION_STATUS_KEY);

            if (transactionStatusModel.getStatus ().trim ().equalsIgnoreCase ( "0" )) {
                failureLayout.setVisibility ( View.GONE );
                successLayout.setVisibility ( View.VISIBLE );
                String aadharCard = transactionStatusModel.getAadharCard ();
                if (transactionStatusModel.getAadharCard () == null) {
                    aadharCard = "N/A";
                } else {
                    if(transactionStatusModel.getAadharCard ().equalsIgnoreCase ( "" )){
                        aadharCard = "N/A";
                    }else {
                        StringBuffer buf = new StringBuffer ( aadharCard );
                        buf.replace ( 0, 10, "XXXX-XXXX-" );
                        System.out.println ( buf.length () );
                        aadharCard = buf.toString ();
                    }
                }

                String bankName = "N/A";
                if (transactionStatusModel.getBankName () != null && !transactionStatusModel.getBankName ().matches ( "" )) {
                    bankName = transactionStatusModel.getBankName ();
                }
                String referenceNo = "N/A";
                if (transactionStatusModel.getReferenceNo () != null && !transactionStatusModel.getReferenceNo ().matches ( "" )) {
                    referenceNo = transactionStatusModel.getReferenceNo ();
                }
                String balance = "N/A";
                if (transactionStatusModel.getBalanceAmount () != null && !transactionStatusModel.getBalanceAmount ().matches ( "" )) {
                    balance = transactionStatusModel.getBalanceAmount ();
                    if (balance.contains ( ":" )) {
                        String[] separated = balance.split ( ":" );
                        balance = separated[ 1 ].trim ();
                    }
                }
                String amount = "N/A";
                if (transactionStatusModel.getTransactionAmount () != null && !transactionStatusModel.getTransactionAmount ().matches ( "" )) {
                    amount = transactionStatusModel.getTransactionAmount ();
                }

                if(transactionStatusModel.getTransactionType ().equalsIgnoreCase ( "Cash Withdrawal" )) {
                    detailsTextView.setText ( transactionStatusModel.getTransactionType () + " for customer account linked with aadhar card " + aadharCard + " " + "was successful. \n \n Bank Name : " + bankName + "\n Reference No : " + referenceNo + " \n " + "Account Balance : " + balance + "\n Transaction Amount : " + amount );
                }else if(transactionStatusModel.getTransactionType ().equalsIgnoreCase ( "Balance Enquery" )){
                    detailsTextView.setText ( transactionStatusModel.getTransactionType () + " for customer account linked with aadhar card " + aadharCard + " " + "was successful. \n \n Bank Name : " + bankName + "\n Reference No : " + referenceNo + " \n " + "Account Balance : " + balance );
                }
            }else{
                failureLayout.setVisibility(View.VISIBLE);
                successLayout.setVisibility(View.GONE);
                failureDetailTextView .setText (transactionStatusModel.getApiComment ());
                failureTitleTextView .setText (transactionStatusModel.getStatusDesc () );
            }
        }
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        okSuccessButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
