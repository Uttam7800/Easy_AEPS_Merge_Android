package com.iserveu.aeps.transaction;



import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * This class represents the Login API, all endpoints can stay here.
 *
 *
 * @author Subhalaxmi Panda
 * @date 22/06/18.
 *
 */

public interface ReportAPI {
    @POST("/aeps/txreports")

    Call<ReportResponse> insertUser(@Header("Authorization") String token,@Body ReportRequest body);
}

