package com.iserveu.aeps.login;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * This class represents the Login API, all endpoints can stay here.
 *
 *
 * @author Subhalaxmi Panda
 * @date 22/06/18.
 *
 */

public interface GetTokenAPI {
    @POST("/getlogintoken")
   // @FormUrlEncoded
    @Headers("Content-Type: application/json")

    /*Call<LoginResponse> insertUser(@Field("username") String username,
                                   @Field("password") String password);

    JSONObject paramObject = new JSONObject();
            paramObject.put("username", "sample@gmail.com");
            paramObject.put("pass", "4384984938943");*/
    Call<GetTokenResponse> getToken(@Body GetTokenRequest body);

}

