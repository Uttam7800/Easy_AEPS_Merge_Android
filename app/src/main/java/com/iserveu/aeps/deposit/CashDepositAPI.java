package com.iserveu.aeps.deposit;



import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * This class represents the Login API, all endpoints can stay here.
 *
 *
 * @author Subhalaxmi Panda
 * @date 22/06/18.
 *
 */

public interface CashDepositAPI {
    @POST("/aeps/transaction/mob/v1")

    Call<CashDepositResponse> checkCashDeposit(@Header("Authorization") String token, @Body CashDepositRequestModel body);
}

