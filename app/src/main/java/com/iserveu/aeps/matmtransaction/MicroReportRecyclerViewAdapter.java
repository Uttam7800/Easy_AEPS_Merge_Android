package com.iserveu.aeps.matmtransaction;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.iserveu.aeps.R;
import com.iserveu.aeps.transaction.ReportModel;
import com.iserveu.aeps.utils.Util;

import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;

public class MicroReportRecyclerViewAdapter extends RecyclerView.Adapter<MicroReportRecyclerViewAdapter.ReportViewhOlder> implements Filterable {

private List<MicroReportModel> reportModelListFiltered;
private List<MicroReportModel> reportModels;
    private IMethodCaller listener;
    private int lastSelectedPosition = -1;


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    reportModelListFiltered = reportModels;
                } else {
                    List<MicroReportModel> filteredList = new ArrayList<> ();
                    for (MicroReportModel row : reportModels) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getRrn ().toLowerCase().contains(charString.toLowerCase()) ||
                                Util.getDateFromTime(Long.parseLong(row.getUpdatedDate())).toLowerCase().contains(charString.toLowerCase()) ||
                                row.getId().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getUserName().toLowerCase().contains(charString.toLowerCase())||
                                row.getStatus().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getApiComment().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getAmountTransacted().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getOperationPerformed().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getTransactionType().toLowerCase().contains(charString.toLowerCase()) ||
                                Util.getDateFromTime(Long.parseLong(row.getCreatedDate())).toLowerCase().contains(charString.toLowerCase()) ||
                                row.getCardNumber().toLowerCase().contains(charString.toLowerCase()) ||
                                row.getApiDate().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    reportModelListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = reportModelListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                reportModelListFiltered = (ArrayList<MicroReportModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public class ReportViewhOlder extends RecyclerView.ViewHolder {

    public ImageView refreshImage;
    public TextView updatedDateTextView;
    public TextView statusTextView,createdDateTextView,apiDateTextView;
    public TextView rrnTextView,transactionTypeTextView,IdTextView,apiCommentTextView,amountTransactedTextView,userNameReportTextView,opertionPerformedTextView,cardNumberTextView;


    public ReportViewhOlder(View view) {
        super ( view );

        refreshImage= view.findViewById ( R.id.refreshImage );
        refreshImage.setVisibility(View.GONE);
        userNameReportTextView= view.findViewById ( R.id.userNameReportTextView );
        updatedDateTextView= view.findViewById ( R.id.updatedDateTextView );
        statusTextView= view.findViewById ( R.id.statusTextView );
        rrnTextView= view.findViewById ( R.id.rrnTextView );
        transactionTypeTextView= view.findViewById ( R.id.transactionTypeTextView );
        IdTextView= view.findViewById ( R.id.IdTextView );
        apiCommentTextView= view.findViewById ( R.id.apiCommentTextView );
        amountTransactedTextView= view.findViewById ( R.id.amountTransactedTextView );
        opertionPerformedTextView= view.findViewById ( R.id.opertionPerformedTextView );
        cardNumberTextView= view.findViewById ( R.id.cardNumberTextView );
        createdDateTextView= view.findViewById ( R.id.createdDateTextView );
        apiDateTextView= view.findViewById ( R.id.apiDateTextView );

        refreshImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v("test","wallet Button : ButtonClicked");
                lastSelectedPosition = getAdapterPosition();
                notifyDataSetChanged();
                listener.refreshMethod(reportModelListFiltered.get(lastSelectedPosition));
            }
        });
    }
}

    public MicroReportRecyclerViewAdapter(List<MicroReportModel> reportModels, IMethodCaller listener) {
        this.reportModels = reportModels;
        this.reportModelListFiltered = reportModels;
        this.listener =listener;

    }

    public MicroReportRecyclerViewAdapter.ReportViewhOlder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.micro_report_row, parent, false);

        return new MicroReportRecyclerViewAdapter.ReportViewhOlder(itemView);
    }

    public void onBindViewHolder(MicroReportRecyclerViewAdapter.ReportViewhOlder holder, int position) {
        MicroReportModel reportModel = reportModelListFiltered.get(position);
        if(reportModel.getStatus().equalsIgnoreCase("INITIATED")){
            holder.refreshImage.setVisibility(View.VISIBLE);
        }else{
            holder.refreshImage.setVisibility(View.GONE);
        }
        if(reportModel.getRrn() != null && !reportModel.getRrn().matches("")){
            holder.rrnTextView.setVisibility(View.VISIBLE);
            holder.rrnTextView.setText("RRN : "+reportModel.getRrn());
        }else{
            holder.rrnTextView.setVisibility(View.GONE);
        }
        if(reportModel.getId() != null && !reportModel.getId().matches("")){
            holder.IdTextView.setVisibility(View.VISIBLE);
            holder.IdTextView.setText("ID : "+reportModel.getId());
        }else{
            holder.IdTextView.setVisibility(View.GONE);
        }
        if(reportModel.getUserName() != null && !reportModel.getUserName().matches("")){
            holder.userNameReportTextView.setVisibility(View.VISIBLE);
            holder.userNameReportTextView.setText("User Name : "+reportModel.getUserName());
        }else{
            holder.userNameReportTextView.setVisibility(View.GONE);
        }
        if(reportModel.getUpdatedDate() != null && !reportModel.getUpdatedDate().matches("")){
            holder.updatedDateTextView.setVisibility(View.VISIBLE);
            holder.updatedDateTextView.setText(Util.getDateFromTime(Long.parseLong(reportModel.getUpdatedDate())));
        }else{
            holder.updatedDateTextView.setVisibility(View.GONE);
        }
        if(reportModel.getStatus() != null && !reportModel.getStatus().matches("")){
            holder.statusTextView.setVisibility(View.VISIBLE);
            holder.statusTextView.setText("Status : "+reportModel.getStatus());
        }else{
            holder.statusTextView.setVisibility(View.GONE);
        }
        if(reportModel.getApiComment() != null && !reportModel.getApiComment().matches("")){
            holder.apiCommentTextView.setVisibility(View.VISIBLE);
            holder.apiCommentTextView.setText("API Comment : "+reportModel.getApiComment());
        }else{
            holder.apiCommentTextView.setVisibility(View.GONE);
        }
        if(reportModel.getAmountTransacted() != null && !reportModel.getAmountTransacted().matches("") && !reportModel.getAmountTransacted().matches("0.0")){
            holder.amountTransactedTextView.setVisibility(View.VISIBLE);
            holder.amountTransactedTextView.setText("Amount Transacted : "+reportModel.getAmountTransacted());
        }else{
            holder.amountTransactedTextView.setVisibility(View.GONE);
        }
        if(reportModel.getOperationPerformed() != null && !reportModel.getOperationPerformed().matches("")){
            holder.opertionPerformedTextView.setVisibility(View.VISIBLE);
            holder.opertionPerformedTextView.setText("Operation Performed : "+reportModel.getOperationPerformed());
        }else{
            holder.opertionPerformedTextView.setVisibility(View.GONE);
        }
        if(reportModel.getTransactionType() != null && !reportModel.getTransactionType().matches("")){
            holder.transactionTypeTextView.setVisibility(View.VISIBLE);
            holder.transactionTypeTextView.setText("Transaction Type : "+reportModel.getTransactionType());
        }else{
            holder.transactionTypeTextView.setVisibility(View.GONE);
        }
        if(reportModel.getCreatedDate() != null && !reportModel.getCreatedDate().matches("")){
            holder.createdDateTextView.setVisibility(View.VISIBLE);
            holder.createdDateTextView.setText("Created Date : "+Util.getDateFromTime(Long.parseLong(reportModel.getCreatedDate())));
        }else{
            holder.createdDateTextView.setVisibility(View.GONE);
        }
        if(reportModel.getCardNumber() != null && !reportModel.getCardNumber().matches("")){
            holder.cardNumberTextView.setVisibility(View.VISIBLE);
            holder.cardNumberTextView.setText("Card Number : "+reportModel.getCardNumber());
        }else{
            holder.cardNumberTextView.setVisibility(View.GONE);
        }
        if(reportModel.getApiDate() != null && !reportModel.getApiDate().matches("")){
            holder.apiDateTextView.setVisibility(View.VISIBLE);
            holder.apiDateTextView.setText("API Date : "+reportModel.getApiDate());
        }else{
            holder.apiDateTextView.setVisibility(View.GONE);
        }
    }

    public int getItemCount() {
        return reportModelListFiltered.size();
    }

    public interface IMethodCaller{
        void refreshMethod (MicroReportModel microReportModel);
    }

}
