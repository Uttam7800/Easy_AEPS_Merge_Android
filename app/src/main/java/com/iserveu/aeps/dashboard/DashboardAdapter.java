package com.iserveu.aeps.dashboard;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.iserveu.aeps.R;
import com.iserveu.aeps.utils.Util;

import java.util.HashMap;
import java.util.List;

import fr.ganfra.materialspinner.MaterialSpinner;

/**
 * Created by USER on 6/23/2018.
 */

public class DashboardAdapter extends BaseExpandableListAdapter {
   private Context _context;

        private List<String> _listDataHeader; // header titles
        // child data in format of header title, child title
        private HashMap<String, List<DashboardChildModel>> _listDataChild;

        public DashboardAdapter(Context context, List<String> listDataHeader,
                                HashMap<String, List<DashboardChildModel>> listChildData) {
            this._context = context;
            this._listDataHeader = listDataHeader;
            this._listDataChild = listChildData;
        }

        @Override
        public Object getChild(int groupPosition, int childPosititon) {
            return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .get(childPosititon);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getChildView(final int groupPosition, final int childPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {

            final DashboardChildModel childText = (DashboardChildModel) getChild(groupPosition, childPosition);

            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate( R.layout.home_list_item, null);
            }


            EditText aadharNumber = (EditText) convertView.findViewById(R.id.aadharNumber);
            EditText mobileNumber = (EditText) convertView.findViewById(R.id.mobileNumber);
            MaterialSpinner bankspinner = (MaterialSpinner) convertView.findViewById(R.id.bankspinner);
            EditText amountEnter = (EditText) convertView.findViewById(R.id.amountEnter);
            EditText apiTidNumber = (EditText) convertView.findViewById(R.id.apiTidNumber);
            ImageView fingerprint = (ImageView) convertView.findViewById(R.id.fingerprint);
            Button submitButton = (Button) convertView.findViewById(R.id.submitButton);


            /*submitButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(_context, BankNameListActivity.class);
                    ((Activity) _context).startActivityForResult(intent, REQUEST_FOR_ACTIVITY_CODE);
                }
            });*/
            childText.setAadharNumberModel (aadharNumber.getText().toString().trim());
            childText.setMobileNumberModel (mobileNumber.getText().toString().trim());
            childText.setAmountEnterModel (amountEnter.getText().toString().trim());
            childText.setApiTidNumberModel (apiTidNumber.getText().toString().trim());

            if (Util.validateAadharNumber ( aadharNumber.getText ().toString ().trim () ) == false) {
                aadharNumber.setError ( "Enter your correct AADHAR Card" );
            }

           /* bankspinner.setText(childText.getBankspinnerModel ());

            if(aadharNumber.getText ()!=null && !aadharNumber.getText ().toString ().trim().matches ("")){
                aadharNumber.setVisibility ( View.VISIBLE );
            }

            if(mobileNumber.getText ()!=null && !mobileNumber.getText ().toString ().trim().matches ("")){
                mobileNumber.setVisibility ( View.VISIBLE );
            }

            if(amountEnter.getText ()!=null && !amountEnter.getText ().toString ().trim().matches ("")){
                amountEnter.setVisibility ( View.VISIBLE );
            }*/

            if (groupPosition == 0){
                aadharNumber.setVisibility ( View.VISIBLE );
                mobileNumber.setVisibility ( View.VISIBLE );
                bankspinner.setVisibility ( View.VISIBLE );
                amountEnter.setVisibility ( View.VISIBLE );
                fingerprint.setVisibility ( View.VISIBLE );
                submitButton.setVisibility ( View.VISIBLE );
                apiTidNumber.setVisibility ( View.GONE );

            }

            if (groupPosition == 1){
                aadharNumber.setVisibility ( View.VISIBLE );
                mobileNumber.setVisibility ( View.VISIBLE );
                bankspinner.setVisibility ( View.VISIBLE );
                amountEnter.setVisibility ( View.VISIBLE );
                fingerprint.setVisibility ( View.VISIBLE );
                submitButton.setVisibility ( View.VISIBLE );
                apiTidNumber.setVisibility ( View.GONE );

            }

             if (groupPosition == 2){
                aadharNumber.setVisibility ( View.VISIBLE );
                mobileNumber.setVisibility ( View.VISIBLE );
                bankspinner.setVisibility ( View.VISIBLE );
                amountEnter.setVisibility ( View.VISIBLE );
                fingerprint.setVisibility ( View.VISIBLE );
                submitButton.setVisibility ( View.VISIBLE );
                 apiTidNumber.setVisibility ( View.GONE );

             }

            if (groupPosition == 3){
                aadharNumber.setVisibility ( View.GONE );
                mobileNumber.setVisibility ( View.GONE );
                bankspinner.setVisibility ( View.GONE );
                amountEnter.setVisibility ( View.GONE );
                fingerprint.setVisibility ( View.VISIBLE );
                apiTidNumber.setVisibility ( View.VISIBLE );
                submitButton.setVisibility ( View.VISIBLE );
            }



            return convertView;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return this._listDataHeader.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return this._listDataHeader.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            String headerTitle = (String) getGroup(groupPosition);
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.home_header, null);
            }

            TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);

            ImageView expandIcon = (ImageView)convertView.findViewById(R.id.expandIcon);

            //lblListHeader.setTypeface(null, Typeface.BOLD);
            lblListHeader.setText(headerTitle);

            if (groupPosition == 0){
                expandIcon.setImageResource(R.drawable.ic_account_balance_black_24dp);

            }
            if (groupPosition == 1){
                expandIcon.setImageResource(R.drawable.ic_attach_money_black_24dp);
            }
            if (groupPosition == 2){
                expandIcon.setImageResource(R.drawable.ic_speaker_black_24dp);
            }
            if (groupPosition == 3){
                expandIcon.setImageResource(R.drawable.ic_cached_black_24dp);
            }

            /*if (isExpanded) {
                // lblListHeader.setTypeface(null, Typeface.BOLD);
                expandIcon.setImageResource(R.drawable.ic_remove);
                //lblListHeader.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_remove, 0, 0, 0);
            } else {
                // If group is not expanded then change the text back into normal
                // and change the icon

                //  lblListHeader.setTypeface(null, Typeface.NORMAL);
                expandIcon.setImageResource(R.drawable.ic_add);
                //lblListHeader.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_add, 0, 0, 0);
            }*/
            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

    }
