/*
 * Copyright 2015, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iserveu.Login;


import com.iserveu.aeps.transaction.ReportContract;
import com.iserveu.aeps.transaction.ReportPresenter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.iserveu.aeps.utils.Constants.TEST_USER_TOKEN;
import static org.mockito.Mockito.verify;

/**
 * Unit tests for the implementation of {@link LoginPresenterTest}.
 */
public class ReportPresenterTest {



    /**
     *  Mock of loginView for unit testing
     */
    @Mock
    private ReportContract.View reportView;



    /**
     *  Mock of ReportPresenter for unit testing
     */
    @InjectMocks
    private ReportPresenter reportPresenter;



    /**
     *  setupReportPresenterTest will be called before execution of the tests
     */

    @Before
    public void setupReportPresenterTest() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // Get a reference to the class under test

        reportPresenter = new ReportPresenter(reportView);


    }

    /**
     *  emptyReportEntryUI() checks fromDate and toDate and token are emptyStrings
     */


    @Test
    public void emptyReportEntryUI() {
        // When the presenter is asked to login with empty field(s)
        reportPresenter.loadReports("","","");
        verify(reportView).emptyDates();



    }

    /**
     *  emptyFromDateUI() checks fromDate is empty String
     */

    @Test
    public void emptyFromDateUI() {
        // When the presenter is asked to login with empty field(s)
        reportPresenter.loadReports("","2018-06-13","");
        verify(reportView).emptyDates();



    }

    /**
     *  emptyToDateUI() checks toDate is empty String
     */

    @Test
    public void emptyToDateUI() {
        // When the presenter is asked to login with empty field(s)
        reportPresenter.loadReports("2018-06-12","","");
        verify(reportView).emptyDates();



    }

    /**
     *  nullFromDateUI() checks fromDate is null
     */

    @Test
    public void nullFromDateUI() {

        // When the presenter is asked to login with invalid username and password
        reportPresenter.loadReports(null,"2018-06-13",TEST_USER_TOKEN);
        verify(reportView).emptyDates();


    }

    /**
     *  nullToDateUI() checks toDate is null
     */


    @Test
    public void nullToDateUI() {

        // When the presenter is asked to login with invalid username and password
        reportPresenter.loadReports("2018-06-12",null,TEST_USER_TOKEN);
        verify(reportView).emptyDates();


    }

    /**
     *  nullTokenUI() checks the token is null
     */


    @Test
    public void nullTokenUI() {

        // When the presenter is asked to login with invalid username and password
        reportPresenter.loadReports("2018-06-12","2018-06-13",null);
        reportView.emptyDates();
        verify(reportView).emptyDates();


    }

    /**
     *  emptyTokenUI() checks the token is empty String
     */

    @Test
    public void emptyTokenUI() {

        // When the presenter is asked to login with invalid username and password
        reportPresenter.loadReports("2018-06-12","2018-06-13","");
        reportView.emptyDates();
        verify(reportView).emptyDates();


    }

    /**
     *  loadReportsList() checks the report fetching is successful
     */

    @Test
    public void loadReportsList() {
        // When the presenter is asked to save an empty note
        reportPresenter.loadReports("2018-06-12", "2018-06-13", TEST_USER_TOKEN);
        reportView.showReports();
        verify(reportView).showReports();

    }

    /**
     *  emptyReportResponseUI() checks the report is an empty list
     */

    @Test
    public void emptyReportResponseUI() {
        // When the presenter is asked to save an empty note
        reportPresenter.loadReports("1970-06-12", "2018-06-13", TEST_USER_TOKEN);
        reportView.showReports();
        verify(reportView).showReports();

    }



}
