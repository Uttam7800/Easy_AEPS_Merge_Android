package com.iserveu.aeps.refund;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RefundRequestModel {
    @SerializedName("transactionId")
    @Expose
    private String transactionId;


    @SerializedName("freshnessFactor")
    @Expose
    private String freshnessFactor;

    public RefundRequestModel(String transactionId, String freshnessFactor) {
        this.transactionId = transactionId;
        this.freshnessFactor = freshnessFactor;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getFreshnessFactor() {
        return freshnessFactor;
    }

    public void setFreshnessFactor(String freshnessFactor) {
        this.freshnessFactor = freshnessFactor;
    }
}
