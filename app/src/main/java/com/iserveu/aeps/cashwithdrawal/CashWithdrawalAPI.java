package com.iserveu.aeps.cashwithdrawal;



import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * This class represents the Login API, all endpoints can stay here.
 *
 *
 * @author Subhalaxmi Panda
 * @date 22/06/18.
 *
 */

public interface CashWithdrawalAPI {
    @POST("/aeps/transaction/mob/v1")

    Call<CashWithdrawalResponse> checkCashWithDrawal(@Header("Authorization") String token, @Body CashWithdrawalRequestModel body);
}

