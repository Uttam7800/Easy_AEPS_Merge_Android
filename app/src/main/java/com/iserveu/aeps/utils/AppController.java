package com.iserveu.aeps.utils;

import android.content.Context;
import android.support.multidex.MultiDex;

import com.finopaytech.finosdk.FinoApplication;
import com.google.firebase.FirebaseApp;

public class AppController extends FinoApplication {

    private static AppController mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(this);
        mInstance = this;
    }
    @Override
    public void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        try {
            MultiDex.install(this);
        } catch (RuntimeException multiDexException) {
            multiDexException.printStackTrace();
        }
    }
    public static synchronized AppController getInstance() {
        return mInstance;
    }
}
