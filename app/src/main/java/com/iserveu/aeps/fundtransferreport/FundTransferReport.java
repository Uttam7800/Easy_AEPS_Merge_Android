package com.iserveu.aeps.fundtransferreport;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.iserveu.aeps.R;
import com.iserveu.aeps.utils.AEPSAPIService;
import com.iserveu.aeps.utils.Constants;
import com.iserveu.aeps.utils.Session;
import com.iserveu.aeps.utils.Util;
import com.rw.loadingdialog.LoadingView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FundTransferReport extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{

    LoadingView loadingView;

    TextView textView;

    private RecyclerView reportRecyclerView;
//    CustomReportAdapter customReportAdapter;
    RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager layoutManager;
    String transactionType = "ISU_FT";
//String transactionType = "RECHARGE";
    private AEPSAPIService apiService;
    Session session;

    ReportFragmentRequest reportFragmentRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fund_transfer_report);

        // hide notification bar
//        getSupportActionBar().hide();

        session = new Session(FundTransferReport.this);

        textView = findViewById(R.id.text_date_picker);
        reportRecyclerView = findViewById ( R.id.reportRecyclerView );
        reportRecyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(FundTransferReport.this);

        reportRecyclerView.setLayoutManager(layoutManager);

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(FundTransferReport.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.show(getFragmentManager(),"jitu");
                dpd.setMaxDate ( Calendar.getInstance () );
                dpd.setAutoHighlight ( true );

            }
        });




    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        String fromdate = year + "-" + (monthOfYear+1) + "-" + dayOfMonth;
        String todate = yearEnd + "-" + (monthOfYearEnd+1)+ "-" + (dayOfMonthEnd);
        String api_todate = yearEnd + "-" + (monthOfYearEnd+1)+ "-" + (dayOfMonthEnd+1);

        String date = dayOfMonth+"/"+(monthOfYear+1)+"/"+year+" To "+(dayOfMonthEnd)+"/"+(monthOfYearEnd+1)+"/"+yearEnd;
        textView.setText ( date );
        Log.d("Report", "fromdate: "+fromdate);
        Log.d("Report", "fromdate todate: "+todate);

        /*ReportFragmentRequest*/ reportFragmentRequest= new ReportFragmentRequest(transactionType,fromdate,api_todate);

        Date from_date=null;
        Date to_date=null;
       SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            from_date = formatter.parse(fromdate);
            to_date = formatter.parse(todate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(from_date.before(to_date)){
            reportCall();
            showLoader();
        }else{
            Toast.makeText(this, "From date should be less than To date.", Toast.LENGTH_SHORT).show();
        }

    }

    private void reportCall() {

//        VerifyBeneRequest verifyBeneRequest = new VerifyBeneRequest(customerIds,getAccountNo,bankName,beneNameEnter,ifscCode,pipeNoo);

        if (this.apiService == null) {
            this.apiService = new AEPSAPIService();
        }

        final ReportFragmentApi reportFragmentApi = this.apiService.getClient().create(ReportFragmentApi.class);

        reportFragmentApi.getReport(session.getUserToken(),reportFragmentRequest, Constants.BASE_URL_report+"transactiondetails").enqueue(new Callback<ReportFragmentResponse>() {
            @Override
            public void onResponse(Call<ReportFragmentResponse> call, Response<ReportFragmentResponse> response) {
//                Log.d("STATUS", "Transaction"+response.body());
//                Log.d("Test", "Transaction: "+"going inside 2255" +transactionAPI);
                if (response.isSuccessful()){
//                                        hideLoader();
                    Log.d("STATUS", "TransactionSuccessfulReport"+response.body());
                    ArrayList<FinoTransactionReports> finoTransactionReports = response.body().getFinoTransactionReports();
                    for( int i=0;i<finoTransactionReports.size();i++) {
                        Log.d("BENILIST", "benilist1111" + finoTransactionReports/*+benilist[i].getFeatureName()+abc[i].getActive()*/);
//                            walletView.showBeniName(benilist[i].getBeneName(),benilist[i].getAccountNo(),benilist[i].getBankName(),benilist[i].getIfscCode());
//                        customReportAdapter = new CustomReportAdapter(finoTransactionReports[i]);
//                        reportRecyclerView.setAdapter(customReportAdapter);
                        mAdapter = new CustomReportAdapter(FundTransferReport.this, finoTransactionReports);

                        reportRecyclerView.setAdapter(mAdapter);
                        hideLoader();
//                        reportRecyclerView.setLayoutManager(new LinearLayoutManager(this));
                    }
//                                        Toast.makeText(getApplicationContext(),"TransactionSuccessful"+response.body(),Toast.LENGTH_LONG).show();
//                    customReportAdapter = new CustomReportAdapter(finoTransactionReports);
//                    reportRecyclerView.setAdapter(customReportAdapter.finoTransactionReports.getId(),customReportAdapter.finoTransactionReports.getApiTid());

//                    hideLoader();
//                    clear();
                }else {
//                                        hideLoader();
                    Log.d("STATUS", "TransactionFailedReport"+response.body());
//                                        Toast.makeText(getApplicationContext(),"TransactionFailed"+response.body(),Toast.LENGTH_LONG).show();
                    hideLoader();
                }
            }

            @Override
            public void onFailure(Call<ReportFragmentResponse> call, Throwable t) {
                Log.d("STATUS", "TransactionFailedReport"+t);
            }
        });
    }

    private void showLoader() {

        if (loadingView ==null){
            loadingView = Util.showProgress(FundTransferReport.this);
        }
        loadingView.show();

    }

    private void hideLoader() {
        if (loadingView!=null){
            loadingView.hide();
        }

    }

}
