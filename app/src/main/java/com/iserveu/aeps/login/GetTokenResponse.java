package com.iserveu.aeps.login;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * This class handles response  of Login Module from login Api
 *
 * @author Subhalaxmi Panda
 * @date 22/06/18.
 */

public class GetTokenResponse {
    @SerializedName("token")
    @Expose
    private String token;


    public String getToken() {
        return token;
    }



    public void setToken(String token) {
        this.token = token;
    }
}
