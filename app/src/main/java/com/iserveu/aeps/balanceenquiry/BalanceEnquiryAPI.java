package com.iserveu.aeps.balanceenquiry;



import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * This class represents the Login API, all endpoints can stay here.
 *
 *
 * @author Subhalaxmi Panda
 * @date 22/06/18.
 *
 */

public interface BalanceEnquiryAPI {
    @POST("/aeps/balanceenquiry/mob/v1")

    Call<BalanceEnquiryResponse> checkBalanceEnquiry(@Header("Authorization") String token, @Body BalanceEnquiryRequestModel body);
}

