package com.iserveu.aeps.refund;

import android.util.Log;

import com.iserveu.aeps.utils.AEPSAPIService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * LoginPresenter class Handle Interaction between Model and View
 *
 *
 * @author Subhalaxmi Panda
 * @date 21/06/18.
 *
 */


public class RefundPresenter implements RefundContract.UserActionsListener {
    /**
     * Initialize LoginView
     */
    private RefundContract.View refundContractView;
    private AEPSAPIService aepsapiService;
    /**
     * Initialize LoginPresenter
     */
    public RefundPresenter(RefundContract.View refundContractView) {
        this.refundContractView = refundContractView;
    }




    @Override
    public void performRefund(String token,RefundRequestModel refundRequestModel) {
        if (refundRequestModel!=null && refundRequestModel.getTransactionId() !=null && !refundRequestModel.getTransactionId().matches("") &&
                refundRequestModel.getFreshnessFactor() !=null && !refundRequestModel.getFreshnessFactor().matches("")
                ) {
            refundContractView.showLoader();

            if (this.aepsapiService == null) {
                this.aepsapiService = new AEPSAPIService();
            }

            // this.aepsapiService = new AEPSAPIService();

            RefundAPI refundAPI =
                    this.aepsapiService.getClient().create(RefundAPI.class);

            refundAPI.checkRefund(token,refundRequestModel).enqueue(new Callback<RefundResponse>() {
                @Override
                public void onResponse(Call<RefundResponse> call, Response<RefundResponse> response) {

                    if(response.isSuccessful()) {
                        // String message = "";
                        if (response.body().getStatus() !=null && !response.body().getStatus().matches("")) {
                            //message = "Login Successful";
                            Log.v("laxmi","hf"+response.body().getStatus());
                            Log.v("laxmi","hf"+response.body().getApiComment());
                            refundContractView.hideLoader();
                            refundContractView.checkRefundStatus(response.body().getStatus(), response.body().getStatusDesc());

                        }else{
                            refundContractView.hideLoader();
                            refundContractView.checkRefundStatus("", "Refund Failed");

                        }

                    }else{
                        refundContractView.hideLoader();
                        refundContractView.checkRefundStatus("", "Refund Failed");

                    }
                }

                @Override
                public void onFailure(Call<RefundResponse> call, Throwable t) {
                    refundContractView.hideLoader();
                    refundContractView.checkRefundStatus("", "Refund Failed");
                }
            });
        } else {
            refundContractView.checkEmptyFields();
        }
    }
}
