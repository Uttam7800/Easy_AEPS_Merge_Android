package com.iserveu.aeps.dashboard;

/**
 * Created by USER on 6/23/2018.
 */

public class DashboardChildModel {



    private String aadharNumberModel;
    private String mobileNumberModel;
    private String bankspinnerModel;
    private String amountEnterModel;
    private String apiTidNumberModel;
    private String withdrawalAadharNumberModel;
    private String withdrawalMobileNumberModel;
    private String withdrawalBankspinnerModel;
    private String withdarawalAmountEnterModel;
    private String balanceaAdharNumberModel;
    private String balanceMobileNumberModel;
    private String balanceBankspinnerModel;
    private String balanceAmountEnterModel;


    public DashboardChildModel() {
        this.aadharNumberModel = aadharNumberModel;
        this.mobileNumberModel = mobileNumberModel;
        this.bankspinnerModel = bankspinnerModel;
        this.amountEnterModel = amountEnterModel;
        this.apiTidNumberModel = apiTidNumberModel;
        this.withdrawalAadharNumberModel = withdrawalAadharNumberModel;
        this.withdrawalMobileNumberModel = withdrawalMobileNumberModel;
        this.withdrawalBankspinnerModel = withdrawalBankspinnerModel;
        this.withdarawalAmountEnterModel = withdarawalAmountEnterModel;
        this.balanceaAdharNumberModel = balanceaAdharNumberModel;
        this.balanceMobileNumberModel = balanceMobileNumberModel;
        this.balanceBankspinnerModel = balanceBankspinnerModel;
        this.balanceAmountEnterModel = balanceAmountEnterModel;
    }

    public String getAadharNumberModel() {
        return aadharNumberModel;
    }

    public void setAadharNumberModel(String aadharNumberModel) {
        this.aadharNumberModel = aadharNumberModel;
    }

    public String getMobileNumberModel() {
        return mobileNumberModel;
    }

    public void setMobileNumberModel(String mobileNumberModel) {
        this.mobileNumberModel = mobileNumberModel;
    }

    public String getBankspinnerModel() {
        return bankspinnerModel;
    }

    public void setBankspinnerModel(String bankspinnerModel) {
        this.bankspinnerModel = bankspinnerModel;
    }

    public String getAmountEnterModel() {
        return amountEnterModel;
    }

    public void setAmountEnterModel(String amountEnterModel) {
        this.amountEnterModel = amountEnterModel;
    }

    public String getApiTidNumberModel() {
        return apiTidNumberModel;
    }

    public void setApiTidNumberModel(String apiTidNumberModel) {
        this.apiTidNumberModel = apiTidNumberModel;
    }

    public String getWithdrawalAadharNumberModel() {
        return withdrawalAadharNumberModel;
    }

    public void setWithdrawalAadharNumberModel(String withdrawalAadharNumberModel) {
        this.withdrawalAadharNumberModel = withdrawalAadharNumberModel;
    }

    public String getWithdrawalMobileNumberModel() {
        return withdrawalMobileNumberModel;
    }

    public void setWithdrawalMobileNumberModel(String withdrawalMobileNumberModel) {
        this.withdrawalMobileNumberModel = withdrawalMobileNumberModel;
    }

    public String getWithdrawalBankspinnerModel() {
        return withdrawalBankspinnerModel;
    }

    public void setWithdrawalBankspinnerModel(String withdrawalBankspinnerModel) {
        this.withdrawalBankspinnerModel = withdrawalBankspinnerModel;
    }

    public String getWithdarawalAmountEnterModel() {
        return withdarawalAmountEnterModel;
    }

    public void setWithdarawalAmountEnterModel(String withdarawalAmountEnterModel) {
        this.withdarawalAmountEnterModel = withdarawalAmountEnterModel;
    }

    public String getBalanceaAdharNumberModel() {
        return balanceaAdharNumberModel;
    }

    public void setBalanceaAdharNumberModel(String balanceaAdharNumberModel) {
        this.balanceaAdharNumberModel = balanceaAdharNumberModel;
    }

    public String getBalanceMobileNumberModel() {
        return balanceMobileNumberModel;
    }

    public void setBalanceMobileNumberModel(String balanceMobileNumberModel) {
        this.balanceMobileNumberModel = balanceMobileNumberModel;
    }

    public String getBalanceBankspinnerModel() {
        return balanceBankspinnerModel;
    }

    public void setBalanceBankspinnerModel(String balanceBankspinnerModel) {
        this.balanceBankspinnerModel = balanceBankspinnerModel;
    }

    public String getBalanceAmountEnterModel() {
        return balanceAmountEnterModel;
    }

    public void setBalanceAmountEnterModel(String balanceAmountEnterModel) {
        this.balanceAmountEnterModel = balanceAmountEnterModel;
    }
}