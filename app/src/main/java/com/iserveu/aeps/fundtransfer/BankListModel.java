package com.iserveu.aeps.fundtransfer;

public class BankListModel {

    private String ACCOUNT_VERAFICATION_AVAILABLE;

    private String PATTERN;

    private String BANKCODE;

    private String IMPS_AVAILABILITY;

    private String NEFT_AVAILABILITY;

    private String BANKNAME;

    private String FLAG;

    public String getACCOUNT_VERAFICATION_AVAILABLE ()
    {
        return ACCOUNT_VERAFICATION_AVAILABLE;
    }

    public void setACCOUNT_VERAFICATION_AVAILABLE (String ACCOUNT_VERAFICATION_AVAILABLE)
    {
        this.ACCOUNT_VERAFICATION_AVAILABLE = ACCOUNT_VERAFICATION_AVAILABLE;
    }

    public String getPATTERN ()
    {
        return PATTERN;
    }

    public void setPATTERN (String PATTERN)
    {
        this.PATTERN = PATTERN;
    }

    public String getBANKCODE ()
    {
        return BANKCODE;
    }

    public void setBANKCODE (String BANKCODE)
    {
        this.BANKCODE = BANKCODE;
    }

    public String getIMPS_AVAILABILITY ()
    {
        return IMPS_AVAILABILITY;
    }

    public void setIMPS_AVAILABILITY (String IMPS_AVAILABILITY)
    {
        this.IMPS_AVAILABILITY = IMPS_AVAILABILITY;
    }

    public String getNEFT_AVAILABILITY ()
    {
        return NEFT_AVAILABILITY;
    }

    public void setNEFT_AVAILABILITY (String NEFT_AVAILABILITY)
    {
        this.NEFT_AVAILABILITY = NEFT_AVAILABILITY;
    }

    public String getBANKNAME ()
    {
        return BANKNAME;
    }

    public void setBANKNAME (String BANKNAME)
    {
        this.BANKNAME = BANKNAME;
    }

    public String getFLAG ()
    {
        return FLAG;
    }

    public void setFLAG (String FLAG)
    {
        this.FLAG = FLAG;
    }

    @Override
    public String toString() {
        return BANKNAME;
    }

}
