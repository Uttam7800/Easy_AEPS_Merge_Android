package com.iserveu.aeps.dashboard;

import android.util.Log;

import com.iserveu.aeps.utils.AEPSAPIService;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BalancePresenter implements BalanceContract.UserActionsListener {
    /**
     * Initialize LoginView
     */
    private BalanceContract.View balanceView;
    private AEPSAPIService aepsapiService;
    /**
     * Initialize LoginPresenter
     */
    public BalancePresenter(BalanceContract.View balanceView) {
        this.balanceView = balanceView;
    }





    @Override
    public void loadBalance(String token) {
        if (token!=null) {

            if (this.aepsapiService == null) {
                this.aepsapiService = new AEPSAPIService();
            }
            balanceView.showLoader ();
            // this.aepsapiService = new AEPSAPIService();

            BalanceApi balanceApi =
                    this.aepsapiService.getClient().create(BalanceApi.class);

            Call<BalanceResponse> respuesta = balanceApi.getBalanceDetails(token);
            respuesta.enqueue(new Callback<BalanceResponse>() {
                @Override
                public void onResponse(Call<BalanceResponse> call, Response<BalanceResponse> response) {
                    if (response.isSuccessful()){
                        String json = response.body().getUserInfoModel().getUserBrand();

                        try {

                            JSONObject obj = new JSONObject(json);

                            if (obj.has("brand")){
                                response.body().getUserInfoModel().setUserBrand(obj.getString("brand"));
                            }

                            //balanceView.showBalance(response.body().getUserInfoModel().getUserBrand());


                        } catch (Throwable t) {
                        }
                        balanceView.hideLoader ();
                        balanceView.showBalance(response.body().getUserInfoModel().getUserBalance());
                        balanceView.showFeature(response.body().getUserInfoModel().getFeatureIdList());
                    }

                }

                @Override
                public void onFailure(Call<BalanceResponse> call, Throwable t) {
                    balanceView.hideLoader ();
                }


            });

        }
    }
}
