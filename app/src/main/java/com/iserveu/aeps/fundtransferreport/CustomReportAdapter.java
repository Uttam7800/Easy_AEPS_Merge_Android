package com.iserveu.aeps.fundtransferreport;

import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.iserveu.aeps.R;
import com.iserveu.aeps.utils.Util;

import java.util.List;

import static java.security.AccessController.getContext;

public class CustomReportAdapter extends RecyclerView.Adapter<CustomReportAdapter.ViewHolder> {

    private FundTransferReport context;
    private List<FinoTransactionReports> finoTransactionReports;
    public CustomReportAdapter(FundTransferReport context , List finoTransactionReports) {
        this.context = context;
        this.finoTransactionReports = finoTransactionReports;
    }

    /*public CustomReportAdapter(Callback<ReportFragmentResponse> callback, FinoTransactionReports[] finoTransactionReports) {

    }

    public CustomReportAdapter(Callback<ReportFragmentResponse> callback, FinoTransactionReports finoTransactionReport) {

    }*/


    @Override
    public ViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fund_transfer_report_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomReportAdapter.ViewHolder holder, int position) {

        holder.itemView.setTag(finoTransactionReports.get(position));

        FinoTransactionReports pu = finoTransactionReports.get(position);

        holder.transactionId.setText(pu.getId());
        holder.accountNumber.setText(pu.getToAccount());
        holder.apiTid.setText(pu.getApiTid());
        holder.apiComment.setText(pu.getApiComment());
        holder.previousAmount.setText(pu.getPreviousAmount());
        holder.amountTransacted.setText(pu.getAmountTransacted());
        holder.currentBalAmt.setText(pu.getBalanceAmount());
        holder.bankName.setText(pu.getBankName());
        holder.beniMobile.setText(pu.getBeniMobile());
        holder.benificiaryName.setText(pu.getBenificiaryName());
        holder.transactionMode.setText(pu.getTransactionMode());
        holder.transactionType.setText(pu.getTransactionType());
        holder.createdDate.setText(Util.getDateFromTime(Long.parseLong(pu.getCreatedDate())));
        holder.gatewaY.setText(pu.getRouteID());
        holder.statuS.setText(pu.getStatus());
        holder.updatedDate.setText(Util.getDateFromTime(Long.parseLong(pu.getUpdatedDate())));
        if (pu.getStatus().equals("SUCCESS")) {
            holder.cardViewFundtransferReport.setBackgroundColor(ContextCompat.getColor(context, R.color.color_report_green));
        }else {
            holder.cardViewFundtransferReport.setBackgroundColor(ContextCompat.getColor(context, R.color.color_report_red));
        }

    }

    @Override
    public int getItemCount() {
        return finoTransactionReports.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView transactionId,accountNumber,apiTid,apiComment,previousAmount,amountTransacted,currentBalAmt,bankName,beniMobile,benificiaryName,transactionMode,transactionType,createdDate,gatewaY,statuS,updatedDate;

        public CardView cardViewFundtransferReport;
//        String date;

        public ViewHolder(View itemView) {
            super(itemView);

            cardViewFundtransferReport = itemView.findViewById(R.id.card_view_fundtransfer_report);


            transactionId = (TextView) itemView.findViewById(R.id.transaction_id);
            accountNumber = (TextView) itemView.findViewById(R.id.account_number);
            apiTid = (TextView) itemView.findViewById(R.id.api_tid);
            apiComment = (TextView) itemView.findViewById(R.id.api_comment);
            previousAmount = (TextView) itemView.findViewById(R.id.previous_amount);
            amountTransacted = (TextView) itemView.findViewById(R.id.amount_transacted);
            currentBalAmt = (TextView) itemView.findViewById(R.id.current_bal_amt);
            bankName = (TextView) itemView.findViewById(R.id.bank_name);
            beniMobile = (TextView) itemView.findViewById(R.id.beni_mobile);
            benificiaryName = (TextView) itemView.findViewById(R.id.benificiary_name);
            transactionMode = (TextView) itemView.findViewById(R.id.transaction_mode);
            transactionType = (TextView) itemView.findViewById(R.id.transaction_type);
            createdDate = (TextView) itemView.findViewById(R.id.created_date);
            gatewaY = (TextView) itemView.findViewById(R.id.gateway);
            statuS = (TextView) itemView.findViewById(R.id.status);
            updatedDate = (TextView) itemView.findViewById(R.id.updated_date);
        }
    }

   /* Context context;
    LayoutInflater inflter;

    FinoTransactionReports finoTransactionReports = new FinoTransactionReports();

    public CustomReportAdapter(Context applicationContext, FinoTransactionReports finoTransactionReports) {
        this.context = applicationContext;
        this.finoTransactionReports = finoTransactionReports;
        inflter = (LayoutInflater.from(applicationContext));
    }

    *//*public CustomReportAdapter(FinoTransactionReports finoTransactionReport) {

    }*//*

    *//*public CustomReportAdapter(Context context, LayoutInflater inflter, FinoTransactionReports finoTransactionReports) {
        this.context = context;
        this.inflter = inflter;
        this.finoTransactionReports = finoTransactionReports;
    }*//*

    *//*public CustomReportAdapter(FinoTransactionReports[] finoTransactionReports) {

    }*//*

    @Override
    public int getCount() {
        return 0;

    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.report_row, null);
//        ImageView icon = (ImageView) view.findViewById(R.id.imageView);
        TextView names = (TextView) view.findViewById(R.id.transaction_id);
        TextView names1 = (TextView) view.findViewById(R.id.account_number);
        TextView names2 = (TextView) view.findViewById(R.id.api_tid);
//        icon.setImageResource(flags[i]);
        names.setText(finoTransactionReports.getId());
        names1.setText(finoTransactionReports.getToAccount());
        names2.setText(finoTransactionReports.getApiTid());
        return view;
    }*/
}
