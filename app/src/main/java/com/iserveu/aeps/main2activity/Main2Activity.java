package com.iserveu.aeps.main2activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.iserveu.aeps.R;
import com.iserveu.aeps.dashboard.BalanceContract;
import com.iserveu.aeps.dashboard.BalancePresenter;
import com.iserveu.aeps.dashboard.DashboardActivity;
import com.iserveu.aeps.dashboard.UserInfoModel;
import com.iserveu.aeps.fundtransfer.FundTransfer;
import com.iserveu.aeps.fundtransferreport.FundTransferReport;
import com.iserveu.aeps.login.LoginContract;
import com.iserveu.aeps.login.LoginPresenter;
import com.iserveu.aeps.microatm.MicroAtmActivity;
import com.iserveu.aeps.utils.Session;
import com.iserveu.aeps.utils.Util;

import java.util.ArrayList;

public class Main2Activity extends AppCompatActivity implements BalanceContract.View, LoginContract.View{

    RelativeLayout fundtransfer_relative_layout,aeps_relative_layout,fundtransfer_report_relative_layout;

    TextView balanceMoney;

    private LoginPresenter loginPresenter;

    private BalancePresenter mActionsListener;
    Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        loginPresenter = new LoginPresenter(Main2Activity.this);

        session = new Session(Main2Activity.this);
        mActionsListener = new BalancePresenter(Main2Activity.this);
        mActionsListener.loadBalance(session.getUserToken());

        balanceMoney = findViewById ( R.id. balanceMoney );

        fundtransfer_relative_layout = findViewById(R.id.fundtransfer_relative_layout);
        fundtransfer_report_relative_layout = findViewById(R.id.fundtransfer_report_relative_layout);
        aeps_relative_layout = findViewById(R.id.aeps_relative_layout);

        fundtransfer_relative_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(Util.haveNetworkConnection(Main2Activity.this)){
                    startActivity(new Intent(getApplicationContext(), FundTransfer.class));
                }else{
                    Toast.makeText(getApplicationContext(),"Oops! Internet connection is not available. Please try again after sometime .",Toast.LENGTH_SHORT).show();
                }
            }
        });

        aeps_relative_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                getLoginDetails(/*session.getUserToken()*/);

                if(Util.haveNetworkConnection(Main2Activity.this)){
                    startActivity(new Intent(getApplicationContext(), DashboardActivity.class));
                }else{
                    Toast.makeText(getApplicationContext(),"Oops! Internet connection is not available. Please try again after sometime .",Toast.LENGTH_SHORT).show();
                }

            }
        });

        fundtransfer_report_relative_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(Util.haveNetworkConnection(Main2Activity.this)){
                    startActivity(new Intent(getApplicationContext(), FundTransferReport.class));
                }else{
                    Toast.makeText(getApplicationContext(),"Oops! Internet connection is not available. Please try again after sometime .",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void showFeature(ArrayList<UserInfoModel.userFeature> userFeatures) {
        String aeps = "27";
        String dmt = "30";
//        String aeps2FeatureCode = "40";
        ArrayList<String> featureCode = new ArrayList<>();
        featureCode.add(aeps);
        featureCode.add(dmt);
//        featureCode.add(aeps2FeatureCode);
//        featurescodesresponse = new ArrayList<>();
        for (int i = 0; i < userFeatures.size(); i++) {
            if(featureCode.contains(userFeatures.get(i).getId())) {
                if(aeps.equalsIgnoreCase(userFeatures.get(i).getId())){
                    aeps_relative_layout.setVisibility(View.VISIBLE);
//                    aepsTabOption.setVisibility(View.VISIBLE);
//                    featurescodesresponse.add(aepsFeatureCode);
                    loginPresenter.performLogin(session.getUserName(),session.getPassword());
                }
                if(dmt.equalsIgnoreCase(userFeatures.get(i).getId())){
                    fundtransfer_relative_layout.setVisibility(View.VISIBLE);
                    fundtransfer_report_relative_layout.setVisibility(View.VISIBLE);
//                    matmTabOption.setVisibility(View.VISIBLE);
//                    featurescodesresponse.add(matmFeatureCode);
                }
//                if(aeps2FeatureCode.equalsIgnoreCase(userFeatures.get(i).getId())){
//                    aeps2Option.setVisibility(View.VISIBLE);
//                    featurescodesresponse.add(aeps2FeatureCode);
//                }
            } else {
                Log.v("radha", "not contains : "+userFeatures.get(i).getId());
            }
        }

    }

    @Override
    public void showBalance(String balance) {
        balanceMoney.setText(balance);

    }

    @Override
    public void emptyLogin() {

    }

    @Override
    public void checkLoginStatus(String status, String message, String token, String nextFreshnessFactor) {

    }

    @Override
    public void showLoginFeature(ArrayList<UserInfoModel.userFeature> userFeatures, String token) {
        loginPresenter.getLoginDetails(token);
    }

    @Override
    public void checkEmptyFields() {

    }

    @Override
    public void showLoader() {

    }

    @Override
    public void hideLoader() {

    }
}