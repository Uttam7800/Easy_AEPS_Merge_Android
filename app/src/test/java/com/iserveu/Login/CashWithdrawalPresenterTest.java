/*
 * Copyright 2015, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.iserveu.Login;


import com.iserveu.aeps.cashwithdrawal.CashWithDrawalContract;
import com.iserveu.aeps.cashwithdrawal.CashWithdrawalPresenter;
import com.iserveu.aeps.cashwithdrawal.CashWithdrawalRequestModel;
import com.iserveu.aeps.utils.Constants;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

/**
 * Unit tests for the implementation of {@link CashWithdrawalPresenterTest}.
 */
public class CashWithdrawalPresenterTest {



    /**
     *  Mock of CashWithDrawalContract.View for unit testing
     */
    @Mock
    private CashWithDrawalContract.View cashWithDrawalContractView;


    /**
     *  Mock of CashWithdrawalPresenter for unit testing
     */

    @InjectMocks
    private CashWithdrawalPresenter cashWithdrawalPresenter;



    /**
     *  setupCashWithdrawalPresenter will be called before execution of the tests
     */

    @Before
    public void setupCashWithdrawalPresenter() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // Get a reference to the class under test

        cashWithdrawalPresenter = new CashWithdrawalPresenter(cashWithDrawalContractView);


    }


    /**
     *  emptyTokenErrorUi() checks whether token is empty string  or not
     */


    @Test
    public void emptyTokenErrorUi() {
        // When the presenter is asked to check cash withdrawal with null request and empty token
        cashWithdrawalPresenter.performCashWithdrawal("",null);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();
    }

    /**
     *  nullTokenErrorUi() checks whether token is null or not
     */

    @Test
    public void nullTokenErrorUi() {
        // When the presenter is asked to check cash withdrawal with null request and token
        cashWithdrawalPresenter.performCashWithdrawal(null,null);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();



    }

    /**
     *  nullRequest() checks whether request model is null or not
     */

    @Test
    public void nullRequest() {
        // When the presenter is asked to check cash withdrawal with null request
        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,null);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();



    }

    /**
     *  emptyAmountRequest() checks whether amount is empty string or not
     */


    @Test
    public void emptyAmountRequest() {
        // When the presenter is asked to check cash withdrawal with empty field(s) like amount
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();



    }

    /**
     *  nullAmountRequest() checks whether amount is null or not
     */

    @Test
    public void nullAmountRequest() {
        // When the presenter is asked to check cash withdrawal with null field(s) like amount
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel(null,"633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();



    }


    /**
     *  emptyAadharNoRequest() checks whether aadhar no is empty String or not
     */


    @Test
    public void emptyAadharNoRequest() {
        // When the presenter is asked to check cash withdrawal with empty field(s) like aadhar no
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();



    }

    /**
     *  nullAadharNoRequest() checks whether aadhar no is null or not
     */

    @Test
    public void nullAadharNoRequest() {
        // When the presenter is asked to check cash withdrawal with null field(s) like aadhar no
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1",null,"20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();



    }


    /**
     *  emptyMobileNoRequest() checks whether mobilenumber is empty or not
     */

    @Test
    public void emptyMobileNoRequest() {
        // When the presenter is asked to check cash withdrawal with empty field(s) like mobile
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();



    }


    /**
     *  nullMobileNoRequest() checks whether mobilenumber is null or not
     */

    @Test
    public void nullMobileNoRequest() {
        // When the presenter is asked to check cash withdrawal with null field(s) like mobile
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100",null,"WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();

    }


    /**
     *  emptyOperationRequest() checks whether operation is empty or not
     */

    @Test
    public void emptyOperationRequest() {
        // When the presenter is asked to check cash withdrawal with empty field(s) like operation
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();



    }


    /**
     *  nullOperationRequest() checks whether operation  is null or not
     */


    @Test
    public void nullOpeartionRequest() {
        // When the presenter is asked to check cash withdrawal with null field(s) like operation
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748",null,"MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();



    }

    /**
     *  emptyFreshnessFactorRequest() checks whether Freshness Factor is empty or not
     */

    @Test
    public void emptyFreshnessFactorRequest() {
        // When the presenter is asked to check cash withdrawal with empty field(s) like freshness factor
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();

    }
    /**
     *  nullFreshnessFactorRequest() checks whether Freshness Factor  is null or not
     */
    @Test
    public void nullFreshnessFactorRequest() {
        // When the presenter is asked to check cash withdrawal with null field(s) like freshness factor
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh",null,"2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();
    }

    /**
     *  emptyCiRequest() checks whether Ci is empty or not
     */

    @Test
    public void emptyCiRequest() {
        // When the presenter is asked to check cash withdrawal with empty field(s) like Ci
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","633735238387","","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();

    }
    /**
     *  nullCiRequest() checks whether Ci  is null or not
     */
    @Test
    public void nullCiRequest() {
        // When the presenter is asked to check cash withdrawal with null field(s) like Ci
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","633735238387",null,"e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();
    }

    /**
     *  emptyDcRequest() checks whether Dc is empty or not
     */
    @Test
    public void emptyDcRequest() {
        // When the presenter is asked to check cash withdrawal with empty field(s) like Dc
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","633735238387","20191230","","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();

    }
    /**
     *  nullDcRequest() checks whether Dc  is null or not
     */
    @Test
    public void nullDcRequest() {
        // When the presenter is asked to check cash withdrawal with null field(s) like Dc
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","633735238387","20191230",null,"0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();
    }

    /**
     *  emptyDpidRequest() checks whether Dpid is empty or not
     */
    @Test
    public void emptyDpidRequest() {
        // When the presenter is asked to check cash withdrawal with empty field(s) like Dpid
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();

    }
    /**
     *  nullDpidRequest() checks whether Dpid  is null or not
     */
    @Test
    public void nullDpidRequest() {
        // When the presenter is asked to check cash withdrawal with null field(s) like Dpid
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332",null,"MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();
    }

    /**
     *  emptyEncryptedPidRequest() checks whether Encryptedpid is empty or not
     */
    @Test
    public void emptyEncryptedPidRequest() {
        // When the presenter is asked to check cash withdrawal with empty field(s) like Encryptedpid
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();

    }
    /**
     *  nullEncryptedPidRequest() checks whether Encryptedpid  is null or not
     */
    @Test
    public void nullEncryptedPidRequest() {
        // When the presenter is asked to check cash withdrawal with null field(s) like Encryptedpid
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL",null,"-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();
    }

    /**
     *  emptyHmacRequest() checks whether hMac is empty or not
     */
    @Test
    public void emptyHmacRequest() {
        // When the presenter is asked to check cash withdrawal with empty field(s) like hMac
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();

    }
    /**
     *  nullHmacRequest() checks whether hMac  is null or not
     */
    @Test
    public void nullHmacRequest() {
        // When the presenter is asked to check cash withdrawal with null field(s) like hMac
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559",null,"607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();
    }

    /**
     *  emptyBankIinRequest() checks whether Bank IIN number is empty or not
     */
    @Test
    public void emptyBankIinRequest() {
        // When the presenter is asked to check cash withdrawal with empty field(s) like Bank IIN number
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();

    }
    /**
     *  nullBankIinRequest() checks whether Bank IIN number  is null or not
     */
    @Test
    public void nullBankIinRequest() {
        // When the presenter is asked to check cash withdrawal with null field(s) like Bank IIN number
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF",null,"MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();
    }

    /**
     *  emptyMcDataRequest() checks whether mcData is empty or not
     */
    @Test
    public void emptyMcDataRequest() {
        // When the presenter is asked to check cash withdrawal with empty field(s) like mcData
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();

    }
    /**
     *  nullMcDataRequest() checks whether mcData  is null or not
     */
    @Test
    public void nullMcDataRequest() {
        // When the presenter is asked to check cash withdrawal with null field(s) like mcData
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152",null,"MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();
    }

    /**
     *  emptyMiRequest() checks whether mi is empty or not
     */
    @Test
    public void emptyMiRequest() {
        // When the presenter is asked to check cash withdrawal with empty field(s) like mi
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();

    }
    /**
     *  nullMiRequest() checks whether mi  is null or not
     */
    @Test
    public void nullMiRequest() {
        // When the presenter is asked to check cash withdrawal with null field(s) like mi
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK",null,"7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();
    }

    /**
     *  emptyRdsIdRequest() checks whether Rdsid is empty or not
     */
    @Test
    public void emptyRdsIdRequest() {
        // When the presenter is asked to check cash withdrawal with empty field(s) like Rdsid
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();

    }
    /**
     *  nullRdsIdRequest() checks whether Rdsid  is null or not
     */
    @Test
    public void nullRdsIdRequest() {
        // When the presenter is asked to check cash withdrawal with null field(s) like Rdsid
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW",null,"1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();
    }

    /**
     *  emptyRdsVerRequest() checks whether Rdsver is empty or not
     */
    @Test
    public void emptyRdsVerRequest() {
        // When the presenter is asked to check cash withdrawal with empty field(s) like Rdsver
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();

    }
    /**
     *  nullRdsVerRequest() checks whether Rdsver  is null or not
     */
    @Test
    public void nullRdsVerRequest() {
        // When the presenter is asked to check cash withdrawal with null field(s) like Rdsver
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001",null,"rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();
    }

    /**
     *  emptySkeyRequest() checks whether Skey is empty or not
     */
    @Test
    public void emptySkeyRequest() {
        // When the presenter is asked to check cash withdrawal with empty field(s) like Skey
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();

    }
    /**
     *  nullSkeyRequest() checks whether Skey  is null or not
     */
    @Test
    public void nullSkeyRequest() {
        // When the presenter is asked to check cash withdrawal with null field(s) like Skey
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0",null);

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();
    }

    /**
     *  emptyAmountAndAadharEmptyRequest() checks whether amount and aadhar both are  empty or not
     */

    @Test
    public void emptyAmountAndAadharEmptyRequest() {
        // When the presenter is asked to check cash withdrawal with empty field(s) like amount , aadhar no
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("","","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();



    }


    /**
     *  emptyAmountAndMobileEmptyRequest() checks whether amount and mobile no both are  empty or not
     */


    @Test
    public void emptyAmountAndMobileEmptyRequest() {
        // When the presenter is asked to check cash withdrawal with empty field(s) like amount , mobile number
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();



    }


    /**
     *  emptyAmountAndOperationEmptyRequest() checks whether amount and operation both are  empty or not
     */

    @Test
    public void emptyAmountAndOpeartionEmptyRequest() {
        // When the presenter is asked to check cash withdrawal with empty field(s) like amount , operation
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();



    }


    /**
     *  emptyAadharAndMobileEmptyRequest() checks whether aadhar and mobile no both are  empty or not
     */


    @Test
    public void emptyAadharAndMobileEmptyRequest() {
        // When the presenter is asked to check cash withdrawal with empty field(s) like aadhar no , mobile number
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();



    }


    /**
     *  emptyAadharAndOperationEmptyRequest() checks whether aadhar and operation both are  empty or not
     */

    @Test
    public void emptyAadharAndOperationEmptyRequest() {
        // When the presenter is asked to check cash withdrawal with empty field(s) like aadhar no , operation
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();



    }

    /**
     *  emptyAadharAndFreshnessFactorEmptyRequest() checks whether aadhar and freshness factor both are  empty or not
     */

    @Test
    public void emptyAadharAndFreshnessFactorEmptyRequest() {
        // When the presenter is asked to check cash withdrawal with empty field(s) like aadhar no , freshness factor
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();



    }

    /**
     *  emptyAmountAndFreshnessFactorEmptyRequest() checks whether aadhar and freshness factor both are  empty or not
     */

    @Test
    public void emptyAmountAndFreshnessFactorEmptyRequest() {
        // When the presenter is asked to check cash withdrawal with empty field(s) like aadhar no , freshness factor
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();



    }


    /*
     *  EmptyRequest() checks whether  amount , aadhar no , operation , mobile number , freshness factor are  empty or not
     */


    @Test
    public void EmptyRequest() {
        // When the presenter is asked to check cash withdrawal with empty field(s) like amount , aadhar no , operation , mobile number
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("","","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","","","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // Then an empty field error is shown in the UI
        verify(cashWithDrawalContractView).checkEmptyFields();
    }


    /**
     * successRequest() checks data whether it is successfully posted
     */


    @Test
    public void successRequest() {
        // When the presenter is asked to load cash withdrawal with dummy success data
        CashWithdrawalRequestModel cashWithdrawalRequestModel = new CashWithdrawalRequestModel("1","633735238387","20191230","e0b84d64-5a1a-444d-b935-7265e5d7da27","0484332","MANTRA.MSIPL","MjAxOC0wNi0yMVQxMzo0MjoyM3UNw/FVrt1f4nLwQAhdh","-4383403874852159559","2G5qB7ATnbRrkohd0oSNibOrpAQw2n5xjngLqiKGv2SSOZuJhsLRpXFwKE9ma0CF","607152","MIIEFjCCAv6gAwIBAgIICI1fLn8m5ewwDQYJKoZIhvcNAQELBQAwgeQxK","MFS100","7377688748","WITHDRAW","MANTRA.WIN.001","1.0.0","rP+Hc9ZdFOULSFf+62JOM9y849fCzyYJXr2KM9+yO9ykWxNk");

        cashWithdrawalPresenter.performCashWithdrawal(Constants.TEST_USER_TOKEN,cashWithdrawalRequestModel);

        // When show Success message in  UI

        cashWithDrawalContractView.checkCashWithdrawalStatus("0","Successful",null);
        // Then show Success message in Test UI
        verify(cashWithDrawalContractView).checkCashWithdrawalStatus("0","Successful",null);



    }
}
